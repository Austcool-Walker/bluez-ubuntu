# Maintainer: Tom Gundersen <teg@jklm.no>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Geoffroy Carrier <geoffroy@archlinux.org>

pkgbase=bluez-ubuntu
_pkgbase=bluez
_pkgname=bluez
pkgname=('bluez-ubuntu' 'bluez-utils-ubuntu' 'bluez-libs-ubuntu' 'bluez-cups-ubuntu' 'bluez-hid2hci-ubuntu' 'bluez-plugins-ubuntu')
pkgver=5.48
patchver=5.48-0ubuntu3
pkgrel=6
url="http://www.bluez.org/"
arch=('x86_64')
license=('GPL2')
provides=('bluez' 'bluez-utils' 'bluez-libs' 'bluez-cups' 'bluez-hid2hci' 'bluez-plugins' 'bluez-ubuntu' 'bluez-utils-ubuntu' 'bluez-libs-ubuntu' 'bluez-cups-ubuntu' 'bluez-hid2hci-ubuntu' 'bluez-plugins-ubuntu')
replaces=('bluez' 'bluez-utils' 'bluez-libs' 'bluez-cups' 'bluez-hid2hci' 'bluez-plugins')
makedepends=('dbus' 'libical' 'systemd' 'alsa-lib' 'ell')
source=(http://archive.ubuntu.com/ubuntu/pool/main/b/${_pkgname}/${_pkgname}_${pkgver}.orig.tar.xz 
		http://archive.ubuntu.com/ubuntu/pool/main/b/${_pkgname}/${_pkgname}_${patchver}.debian.tar.xz
        bluetooth.modprobe
        refresh_adv_manager_for_non-LE_devices.diff)
sha512sums=('SKIP'
            'SKIP'
            'SKIP'
            'SKIP')

prepare() {
	cd "$srcdir"/${_pkgname}-${pkgver}
#	cp -v "${BUILDDIR}/series" "${srcdir}/debian/patches/series"
      for i in $(grep -v '#' ${srcdir}/debian/patches/series); do
        patch -p1 -i "${srcdir}/debian/patches/${i}"
    done
#  cd "$srcdir"/${_pkgname}-${pkgver}
#  patch -Np1 -i ../refresh_adv_manager_for_non-LE_devices.diff
}

build() {
  cd "$srcdir"/${_pkgname}-${pkgver}
  ./configure CC='/usr/bin/clang' \
          --prefix=/usr \
          --mandir=/usr/share/man \
          --sysconfdir=/etc \
          --localstatedir=/var \
          --libexecdir=/usr/lib \
          --with-dbusconfdir=/usr/share \
          --enable-sixaxis \
          --enable-mesh \
          --enable-experimental \
          --enable-library # this is deprecated
  make
}

#check() {
#  cd "$srcdir"/${_pkgname}-${pkgver}
#  make check || /bin/true # https://bugzilla.kernel.org/show_bug.cgi?id=196621
#}


package_bluez-ubuntu() {
  pkgdesc="Daemons for the bluetooth protocol stack"
  depends=('libical' 'dbus' 'glib2' 'alsa-lib')
  backup=('etc/bluetooth/main.conf')
  conflicts=('obexd-client' 'obexd-server')

  cd "$srcdir"/${_pkgname}-${pkgver}
  make DESTDIR=${pkgdir} \
       install-libexecPROGRAMS \
       install-dbussessionbusDATA \
       install-systemdsystemunitDATA \
       install-systemduserunitDATA \
       install-dbussystembusDATA \
       install-dbusDATA \
       install-man8

  # ship upstream main config file
  install -dm755 ${pkgdir}/etc/bluetooth
  install -Dm644 ${srcdir}/${_pkgbase}-${pkgver}/src/main.conf ${pkgdir}/etc/bluetooth/main.conf

  # add basic documention
  install -dm755 ${pkgdir}/usr/share/doc/${_pkgbase}/dbus-apis
  cp -a doc/*.txt ${pkgdir}/usr/share/doc/${_pkgbase}/dbus-apis/
  # fix module loading errors
  install -dm755 ${pkgdir}/usr/lib/modprobe.d
  install -Dm644 ${srcdir}/bluetooth.modprobe ${pkgdir}/usr/lib/modprobe.d/bluetooth-usb.conf
  # load module at system start required by some functions
  # https://bugzilla.kernel.org/show_bug.cgi?id=196621
  install -dm755 $pkgdir/usr/lib/modules-load.d
  echo "crypto_user" > $pkgdir/usr/lib/modules-load.d/bluez.conf
  
  # fix obex file transfer - https://bugs.archlinux.org/task/45816
  ln -fs /usr/lib/systemd/user/obex.service ${pkgdir}/usr/lib/systemd/user/dbus-org.bluez.obex.service
}

package_bluez-utils-ubuntu() {
  pkgdesc="Development and debugging utilities for the bluetooth protocol stack"
  depends=('dbus' 'systemd' 'glib2')
  optdepends=('ell: for btpclient')
  conflicts=('bluez-hcidump')
  provides=('bluez-hcidump' 'bluez-hcidump-ubuntu')
  replaces=('bluez-hcidump' 'bluez<=4.101')

  cd "$srcdir"/${_pkgname}-${pkgver}
  make DESTDIR=${pkgdir} \
       install-binPROGRAMS \
       install-man1

  # add missing tools FS#41132, FS#41687, FS#42716
  for files in `find tools/ -type f -perm -755`; do
    filename=$(basename $files)
    install -Dm755 ${srcdir}/${_pkgbase}-${pkgver}/tools/$filename ${pkgdir}/usr/bin/$filename
  done
  
  # libbluetooth.so* are part of libLTLIBRARIES and binPROGRAMS targets
  #make DESTDIR=${pkgdir} uninstall-libLTLIBRARIES
  #rmdir ${pkgdir}/usr/lib
  rm -rf ${pkgdir}/usr/lib
  
  # move the hid2hci man page out
  mv ${pkgdir}/usr/share/man/man1/hid2hci.1 ${srcdir}/
}

package_bluez-libs-ubuntu() {
  pkgdesc="Deprecated libraries for the bluetooth protocol stack"
  depends=('glibc')
  license=('LGPL2.1')

  cd "$srcdir"/${_pkgname}-${pkgver}
  make DESTDIR=${pkgdir} \
       install-includeHEADERS \
       install-libLTLIBRARIES \
       install-pkgconfigDATA
}

package_bluez-cups-ubuntu() {
  pkgdesc="CUPS printer backend for Bluetooth printers"
  depends=('cups')

  cd "$srcdir"/${_pkgname}-${pkgver}
  make DESTDIR=${pkgdir} install-cupsPROGRAMS
}

package_bluez-hid2hci-ubuntu() {
  pkgdesc="Put HID proxying bluetooth HCI's into HCI mode"
  depends=('systemd')

  cd ${_pkgbase}-${pkgver}
  make DESTDIR=${pkgdir} \
       install-udevPROGRAMS \
       install-rulesDATA
  
  install -dm755 ${pkgdir}/usr/share/man/man1
  mv ${srcdir}/hid2hci.1 ${pkgdir}/usr/share/man/man1/hid2hci.1
}

package_bluez-plugins-ubuntu() {
  pkgdesc="bluez plugins (PS3 Sixaxis controller)"
  depends=('systemd')

  cd "$srcdir"/${_pkgname}-${pkgver}
  make DESTDIR=${pkgdir} \
       install-pluginLTLIBRARIES
}
